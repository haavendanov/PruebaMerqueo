<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Item;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('user')->get();

        return response()->json($items, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file_content = file_get_contents($request->file('file'));
        $lines = explode("\n",$file_content);
        
        $index_greater = 0;
        $number_of_letters_uppercase = 0;

        foreach ($lines as $index => $line) {
            $mixed_case = $line;
            $lower_case = strtolower($mixed_case);
            $similar = similar_text($mixed_case, $lower_case);

            $letters = strlen($mixed_case) - $similar;
            if($letters > $number_of_letters_uppercase)
                $index_greater = $index;
        }

        $item = new Item;
        $item->letters = $lines[$index_greater];
        $item->user_id = Auth::user()->id;
        $item->save();

        return response()->json()->setStatusCode(200);

    }
}
