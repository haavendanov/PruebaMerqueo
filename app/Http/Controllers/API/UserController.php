<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

use Validator;

class UserController extends Controller
{
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('Merqueo')->accessToken;
            $success['name'] = $user->name;
            return response()->json($success, 200);
        } else {
            return response()->json()->setStatusCode(401);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'document_type' => 'required',
            'document_number' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) 
            return response()->json($validator->errors(), 400);

        $user = new User;
        $user->name = $request->name;
        $user->document_type = $request->document_type;
        $user->document_number = $request->document_number;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $success['token'] = $user->createToken('Merqueo')->accessToken;

        return response()->json($success, 201);
    }
}
