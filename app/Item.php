<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'letters', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
